﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BasicCounterClass;

namespace BasicCounterTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            BasicCounterProgram.total = 7;
            Assert.AreEqual(6, BasicCounterProgram.Decrementation());
            Assert.AreEqual(7, BasicCounterProgram.Incrementation());
            Assert.AreEqual(0, BasicCounterProgram.Remise());

            BasicCounterProgram.total = 20;
            Assert.AreEqual(21, BasicCounterProgram.Incrementation());
            Assert.AreEqual(20, BasicCounterProgram.Decrementation());
            Assert.AreEqual(0, BasicCounterProgram.Remise());
        }
    }
}
