﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form 重写 Dispose，以清理组件列表。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Windows 窗体设计器所必需的
    Private components As System.ComponentModel.IContainer

    '注意: 以下过程是 Windows 窗体设计器所必需的
    '可以使用 Windows 窗体设计器修改它。  
    '不要使用代码编辑器修改它。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Plus = New System.Windows.Forms.Button()
        Me.Total = New System.Windows.Forms.Label()
        Me.Moins = New System.Windows.Forms.Button()
        Me.Raz = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'Plus
        '
        Me.Plus.Font = New System.Drawing.Font("Cambria", 28.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Plus.Location = New System.Drawing.Point(81, 212)
        Me.Plus.Name = "Plus"
        Me.Plus.Size = New System.Drawing.Size(150, 100)
        Me.Plus.TabIndex = 0
        Me.Plus.Text = "+"
        Me.Plus.UseVisualStyleBackColor = True
        '
        'Total
        '
        Me.Total.AutoSize = True
        Me.Total.Font = New System.Drawing.Font("Cambria Math", 48.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Total.Location = New System.Drawing.Point(284, -18)
        Me.Total.Name = "Total"
        Me.Total.Size = New System.Drawing.Size(232, 446)
        Me.Total.TabIndex = 1
        Me.Total.Text = "0"
        '
        'Moins
        '
        Me.Moins.Font = New System.Drawing.Font("Cambria", 28.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Moins.Location = New System.Drawing.Point(536, 212)
        Me.Moins.Name = "Moins"
        Me.Moins.Size = New System.Drawing.Size(150, 100)
        Me.Moins.TabIndex = 2
        Me.Moins.Text = "-"
        Me.Moins.UseVisualStyleBackColor = True
        '
        'Raz
        '
        Me.Raz.Font = New System.Drawing.Font("Cambria", 28.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Raz.Location = New System.Drawing.Point(311, 297)
        Me.Raz.Name = "Raz"
        Me.Raz.Size = New System.Drawing.Size(150, 100)
        Me.Raz.TabIndex = 3
        Me.Raz.Text = "RAZ"
        Me.Raz.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Cambria", 25.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(327, 64)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(116, 52)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "Total"
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(800, 450)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Raz)
        Me.Controls.Add(Me.Moins)
        Me.Controls.Add(Me.Total)
        Me.Controls.Add(Me.Plus)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Plus As Button
    Friend WithEvents Total As Label
    Friend WithEvents Moins As Button
    Friend WithEvents Raz As Button
    Friend WithEvents Label1 As Label
End Class
