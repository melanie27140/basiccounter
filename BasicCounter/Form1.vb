﻿Imports BasicCounterClass

Public Class Form1
    Private Sub Plus_Click(sender As Object, e As EventArgs) Handles Plus.Click
        Total.Text = BasicCounterProgram.Incrementation()
    End Sub

    Private Sub Moins_Click(sender As Object, e As EventArgs) Handles Moins.Click
        Total.Text = BasicCounterProgram.Decrementation()
    End Sub

    Private Sub Raz_Click(sender As Object, e As EventArgs) Handles Raz.Click
        Total.Text = BasicCounterProgram.Remise()
    End Sub
End Class
